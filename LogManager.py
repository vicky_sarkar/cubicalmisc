#!/usr/bin/python
import time
import subprocess
from datetime import datetime
from datetime import date
import shutil
import os
from Config import LOG_FILES 


todaysDate = str(date.today())
def initialCheck(filePath,rotatePath):
    if(os.path.exists(rotatePath)):   
        p = subprocess.Popen(["stat", "-c","%y", rotatePath], stdout=subprocess.PIPE, stderr = subprocess.PIPE)
        output, error = p.communicate()
        stat = output
        lastDateModified = str(stat.partition(' ')[0])
        if(lastDateModified != todaysDate):
            everyDayChangeWorker("sudo logrotate -v -f /etc/logrotate.d/cubical")
            print "Day Has Changed"
        if(lastDateModified == todaysDate):
            shutil.copy2(rotatePath,filePath)
            print "Master Has Restarted : File Copied ..."
            #print rotatePath 
            #print filePath

def everyFifteenMinutesCheck(filePath , backUpLogPath):
    print "Hi I am Called After 15 Mins "
    backUpLog = str(backUpLogPath)
    filepath  = str(filePath)
    print filepath
    print backUpLog
    shutil.copy2(filePath, backUpLog) # copying ( /var/log is a Temprorary File (in RAM) ) 
    print "BACK UP LOG SUCCESSFULLY COPIED "

def everyDayChangeWorker(forceLogRotatePath):
    os.system(forceLogRotatePath)
    


#stat -c "%y" /Cubical/backUp.log  "Gives Output = "date time TimeZone" " , That is splited to lastDateModified .
#Infinite loop for handling BackUp Logs ... 
#fileNamePath = str(" ")
#for AllKeyValue in range(len(LOG_FILES)):
# for key, value in LOG_FILES[AllKeyValue].items():
#    if(str(key) == "rotatepath"):
#      initialCheck(fileNamePath , str(value))
      #print fileNamePath
#    if(str(key) == "filepath"):
#      fileNamePath = str(value)
#      print fileNamePath

for dict in LOG_FILES:
    #print dict
    if "rotatepath" in dict and "filepath" in dict:
        print dict['filepath'], dict['rotatepath']
        initialCheck(dict['filepath'], dict['rotatepath'])

while True:
   currentMinute = datetime.now().minute
   print currentMinute
   if(currentMinute%15 == 0):
     for dict in LOG_FILES:
         #print dict
         if "rotatepath" in dict and "filepath" in dict:
            print dict['filepath'], dict['rotatepath']
            #initialCheck(dict['filepath'], dict['rotatepath'])
            everyFifteenMinutesCheck(str(dict['filepath']), str(dict['rotatepath']))
   
   dynamicDate = str(date.today())
   
   if(dynamicDate != todaysDate):
     #everyDayChangeWorker("sudo logrotate -v -f /etc/logrotate.d/cubical")
     print "Day Has Changed Just Now ..."
     todaysDate = str(date.today())
   
   time.sleep(60)   
   
