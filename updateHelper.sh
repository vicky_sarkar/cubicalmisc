#!/bin/bash
#/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/local/games:/usr/games

deleteThisFile=false # Flag Which Checks That Whether this Script Should Delete or Not ...

# Copying new Fstab Only if , Change doesn't exist ...
sudo grep -q "tmpfs /var/log tmpfs defaults,noatime,nosuid,mode=0755,size=10m" "/etc/fstab" || sudo su -c "echo 'tmpfs /var/log tmpfs defaults,noatime,nosuid,mode=0755,size=10m' >> /etc/fstab"

#Checking Difference in Two below Files , if they differ Deleteing this script else copying new crontab to older one ...
diff /Cubical/data/replaceCrontab/newCrontab /etc/crontab >/dev/null 2>&1
if [ $? -eq 0 ]
then
    echo "Files are the same"
    deleteThisFile=true 
    # As Files Are same, Copying must have completed before ; so no need to Execute this script again & again , therefore Reversing Flag .. 
else
    echo "Files are different"
    sudo cp /Cubical/data/replaceCrontab/newCrontab /etc/crontab
    echo "Files Copied Successfully"
fi

# if Flag Reversed , This Script File will Delete ...
if [ "$deleteThisFile" =  true ]
then
   echo "Deleting File updateHelper.sh"
   sudo rm -rf /Cubical/scripts/updateHelper.sh
fi

